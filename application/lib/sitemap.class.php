<?php

namespace skf;

class sitemap extends \DomDocument{

	private $root, $url, $media;

	/**
	 * Constructor
	 * Set up the root node
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->encoding = 'UTF-8';
		$this->formatOutput = true;
		$this->preserveWhiteSpace = false;
 		// create the root element
		$this->root = $this->appendChild( $this->createElement( 'urlset' ) );
		$this->root->setAttribute( 'xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
	}

	/**
	 * Set up a new url element object
	 * @access	public
	 *
	 */
	public function newUrl()
	{
		$this->url = $this->createElement( "url" );
	}

	/**
	 * Add an element to the sitemap
	 * @access	public
	 * @param	string	$name
	 * @param	mixed	$value
	 *
	 */
	public function add( $name, $value=null )
	{
		switch( $name )
		{
			case 'video':
			case 'image':
			$this->media = $this->createElement( "$name:$name" );
			$this->url->appendChild( $this->media );
			$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');
			$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:video', 'http://www.google.com/schemas/sitemap-video/1.1');
			break;	

			case 'video:player_loc':
				$element = $this->createElement( "$name" );
				$this->media->appendChild( $element );
				$text_node = $this->createTextNode( $value[0] );
				$element->appendChild( $text_node );

				$attribute = $this->createAttribute( 'allow_embed' );
				$attribute->value = $value[1];
				$element->appendChild( $attribute );

				$attribute = $this->createAttribute( 'autoplay' );
				$attribute->value = 'ap='.$value[2];
				$element->appendChild( $attribute );

			break;

			case 'image:loc':
			case 'image:caption':
			case 'video:title':
			case 'video:content_loc':
			case 'video:description':
			case 'video:thumbnail_loc':
				$element = $this->createElement( "$name" );
				$this->media->appendChild( $element );
				$text_node = $this->createTextNode( $value );
				$element->appendChild( $text_node );
			break;

			default:
			$element = $this->createElement( $name );
			$this->url->appendChild( $element );
			$text_node = $this->createTextNode( $value );
			$element->appendChild( $text_node );

			break;
		}
		$this->root->appendChild( $this->url );
	}


	/**
	 * Returns a string representation of the class
	 *
	 * @access	public
	 * return	string
	 *
	 */
	public function __toString()
	{
		return $this->saveXML();
	}

} // end of class

/*** 
 * Example

$map = new sitemap();

$url = $map->newUrl();
$map->add( 'loc', 'https://example.com/blog/456/foo' );
$map->add( 'lastmod', '2018-06-01' );
$map->add( 'priority', '0.8' );
$map->add( 'changefreq', 'daily' );

$url = $map->newUrl();
$map->add( 'loc', 'https://example.com/blog/123/foo' );
$map->add( 'lastmod', '2018-04-28' );
$map->add( 'priority', '0.8' );
$map->add( 'changefreq', 'daily' );


$url = $map->newUrl();
$map->add( 'loc', 'https://example.com/blog/890/foo' );
$map->add( 'image' );
$map->add( 'image:loc', 'https://example.com/images/pic.jpg' );
$map->add( 'image:caption', 'Johnny Rotten' );

$map->add( 'video' );
$map->add( 'video:title', 'God Save the Queen' );
$map->add( 'video:description', 'Footage of Pistols' );
// three options for player location array( player_url, allow_embed, auto_play )
$map->add( 'video:player_loc', array( 'http://www.example.com/videoplayer.swf?video=123', 'yes', 1 ) );
$map->add( 'video:thumbnail_loc', 'http://example.com/video/thumbnail.jpg' );
$map->add( 'video:content_loc', 'http://www.example.com/video123.flv' );
echo $map;
**/

?>
