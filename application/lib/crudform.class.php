<?php

namespace skf;

class crudform{

	public $errors = [];
	public $form;
	public $validation;

	public function __construct( form $form, formField $formField, validation $validation )
	{
		$this->form = $form;
		$this->formField = $formField;
		$this->validation =  $validation;
	}

	public function build()
	{
                $this->form->create();
                $this->form->id ='dao_form';
                $this->form->class ='form';
                $this->form->method = 'post';
                $this->form->action = '/crud';

                // create a fieldset
                $fs1 = new formField( 'fieldset' );
                $fs1->add( 'fieldset' );
                $fs1->id = 'fs1';
                // create legend
                $legend = new formField( 'legend', 'Select Database Table' );
                $legend->for = 'table_name';
                // add the legend to the fieldset
                $fs1->addField( $legend );
                // create the dropdown
                $options = new formField( 'select' );
                $options->id = 'table_name';
                $options->title = 'Table name must be alpha numeric and underscore only';
                $options->maxlength = 3;
                $options->minlength = 15;
                $options->name = 'table_name';
                $options->autofocus = 'true';
                $options->required = 'required';

                foreach( $tables as $t )
                {
                        $opt = new formField( 'option', $t);
                        $opt->id = $t;
                        $opt->value = $t;
                        $options->addField( $opt );
                }
                $fs1->addField( $options );

                // create legend
                $legend = new formField( 'legend', 'Enter Existing Controller (Optional)' );
                $legend->for = 'existing_controller';
                // add the legend to the fieldset
                $fs1->addField( $legend );

                // create an input field which is required
                $field = new formField( 'input' );
                $field->type = 'text';
                // $field->required = 'required';
                $field->name = 'existing_controller';
                $field->title = 'Table name must be alpha numeric and underscore only';
                $field->min = 3;
                $field->max = 15;

                $field->id = 'existing_controller';
                // add the input field to the fieldset
                $fs1->addField( $field );

                // create the submit button
                $submit = new formField( 'input' );
                $submit->type="submit";
                $submit->value="Create Crud";
                $submit->id="submitButton";
                // add the fieldset to the form, boom!
                $fs1->addField( $submit );

                $this->form->addField( $fs1 );
	}

	public function isValid()
	{
		return $this->validation->isValid();
	}

	public function __toString()
	{
		return $this->form->saveXML();
	}
}
