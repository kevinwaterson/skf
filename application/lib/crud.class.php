<?php

namespace skf;

class crud{

	public $errors = [];

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	Db object	$db
	 * @return	void
	 *
	 */
	public function __construct( Db $db )
	{
		$this->db = $db;
	}

	/**
	 * Create the crud files
	 *
	 * @access	public
	 * @param	string	$table_name	The database table name
	 * @param	string	$module_name	Optional name of module to write to
	 * @return	bool
	 *
	 */
	public function create( string $table_name, string $module_name=null ):bool
	{
		// set the module to write to
		$module_name = is_null( $module_name ) ? $table_name : $module_name;

		$add = 'callAdd'.$module_name;
		$edit = 'callEdit'.$module_name;
		$update = 'callUpdate'.$module_name;
		$delete = 'callDelete'.$module_name;

		echo 'writing to '.$module_name;
		return true;
	}

	/**
	 * Use reflection api to check if method exists
	 *
	 * @access	private
	 * @param	string	$class_name
	 * @param	string	$method_name
	 * @return	bool
	 *
	 */
	public function isCrudMethod( string $class_name, string $method_name ):bool
	{
		$class = new $class_name;
		$reflection = new ReflectionClass( $class_name );
		return true;
	}

	/**
	 *
	 * Returns a HTML list of errors
	 *
	 * @access	public
	 * @return	string
	 *
	 */
	public function errorList():string
	{
		$ret = '<ul>';
		foreach( $this->errors as $err )
		{
			$ret .= "<li>$err</li>";
		}
		return $ret;
	}

	/**
	 * Checks if there are any errors
	 *
	 * @access	public
	 * @return	bool
	 *
	 */
	public function isValid():bool
	{
		return sizeof( $this->errors ) > 0 ;
	}

} // end of class
