CREATE TABLE contact(
	id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(150) NOT NULL,
	message TEXT NOT NULL,
	last_updated TIMESTAMP
);

INSERT INTO contact (email, message) VALUES ('kevin@example.com', 'This is not a love song' );
INSERT INTO contact (email, message) VALUES ('kevin@example.com', 'This is not a love song' );
INSERT INTO contact (email, message) VALUES ('kevin@example.com', 'This is not a love song' );
INSERT INTO contact (email, message) VALUES ('kevin@example.com', 'This is not a love song' );
INSERT INTO contact (email, message) VALUES ('kevin@example.com', 'This is not a love song' );
