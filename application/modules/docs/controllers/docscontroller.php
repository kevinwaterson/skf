<?php

namespace skf;

class docsController extends baseController implements IController
{
	/**
	*
	* Constructor, duh
	*
	*/

	public function __construct( View $view, Uri $uri, Config $config )
	{
		parent::__construct( $view, $uri );
		
		$this->view->current_menu_item = 'about';
		$this->view->version = $config->config_values['application']['version'];

		// set the template directory
		$this->tpl = new view;
		$this->tpl->setTemplateDir(APP_PATH.'/modules/docs/views');

		$this->view->current_menu_item = 'docs';
	}

	/**
	*
	* The index function displays the login form
	*
	*/
	public function callIndex()
	{
		/*** a view variable ***/
		$this->view->title = 'SKF Blog';
		$this->view->heading = 'SKF Blog';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'docs/index' );

		/*** fetch the template ***/
		$this->content = $this->tpl->fetch( 'index.phtml', $cache_id);
	}

	public function callSitemap()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Sitemap';
                $this->view->heading = 'Sitemap';

                /*** the cache id is based on the file name ***/
                $cache_id = md5( 'docs/sitemap.php' );

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'sitemap.phtml', $cache_id);

	}


	public function callConfiguration()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Configuration';
                $this->view->heading = 'Configuration';

                /*** the cache id is based on the file name ***/
                $cache_id = md5( 'docs/configuration.php' );

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'configuration.phtml', $cache_id);

	}


        public function callLibs()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF libs';
                $this->view->heading = 'Libs';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'libs.phtml' );
        }



        public function callConstants()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Constants';
                $this->view->heading = 'Constants';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'constants.phtml' );
        }


        public function callControllers()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Controllers';
                $this->view->heading = 'Controllers';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'controllers.phtml' );
        }


        public function callDatabase()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Database';
                $this->view->heading = 'Database';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'database.phtml' );
        }


        public function callHtaccess()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF htaccess';
                $this->view->heading = '.htaccess';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'htaccess.phtml' );
        }

        public function callRss()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF RSS';
                $this->view->heading = 'RSS';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'rss.phtml' );
        }

        public function callMail()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Mail';
                $this->view->heading = 'Mail';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'mail.phtml' );
        }


        public function callLanguage()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Language';
                $this->view->heading = 'Language';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'language.phtml' );
        }

        public function callLogging()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Logging';
                $this->view->heading = 'Logging';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'logging.phtml' );
        }

        public function callNamespaces()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Namespaces';
                $this->view->heading = 'Namespaces';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'namespaces.phtml' );
        }


        public function callForms()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Forms';
                $this->view->heading = 'Forms';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'forms.phtml' );
        }

        public function callModules()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Modules';
                $this->view->heading = 'Modules';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'modules.phtml' );
        }

        public function callCaching_and_templating()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Caching and Templating';
                $this->view->heading = 'Caching and Templating';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'caching_and_templating.phtml' );
        }

        public function callUri()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF URI';
                $this->view->heading = 'URI';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'uri.phtml' );
        }

        public function callValidation()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF Validation';
                $this->view->heading = 'Validation';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'validation.phtml' );
        }

        public function callView()
        {
                /*** a view variable ***/
                $this->view->title = 'SKF View';
                $this->view->heading = 'View';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'view.phtml' );
        }

	public function callSnorg()
	{
		$this->view->title = 'SKF View';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'snorg.phtml' );
	}

	public function callEvents()
	{
		$this->view->title = 'SKF Events';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'events.phtml' );
	}

	public function callAssets()
	{
		$this->view->title = 'SKF Assets';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'assets.phtml' );
	}

	public function callLayouts()
	{
		$this->view->title = 'SKF Layouts';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'layouts.phtml' );
	}

	public function callDependency()
	{
		$this->view->title = 'SKF Dependency Injection';
		$this->view->heading = 'Dependency Injection';
		$this->content = $this->tpl->fetch( 'dependency.phtml' );
	}


} // end of class
